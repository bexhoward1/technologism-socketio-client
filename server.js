var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function (socket){
  console.log('Connected');
  socket.on('Ascii', function (msg) {
    console.log('Msg: ', msg);
  });
});

http.listen(3002, function () {
  console.log('listening on *:3002');
});

